QuizTool
クイズができます。

@使用ライブラリ

	・Siv3d(グラフィック)
	・cURL(web)

@インストール
	・実行するにはSiv3Dライブラリがたぶん必要です。下記サイトに沿って入れてください。

		http://play-siv3d.hateblo.jp/entry/install

	・exeから起動する場合,exeがあるフォルダと同じ階層に

　		・もともとexeが入っていた場所にあった*.dllファイル
　		・Engineフォルダ
　		・Assetsフォルダ
		・IntroGameSoundフォルダ

　	を入れてください

@How to use

	Z...次の問題へ
	X...正解
	C...不正解
	V...再開
	Q...終了
	1~9...名前変更
