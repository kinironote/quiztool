﻿#include <Siv3D.hpp>
#include <curl/curl.h>
#include <math.h>
#include <string>
#include <Windows.h>
#include <string>

#include "IntroQuiz.h"


IntroQuiz::IntroQuiz()
{
}


IntroQuiz::~IntroQuiz()
{
}

//Arduinoからの応答(1byte)を受け取る
bool IntroQuiz::arduinoGet(s3d::Serial *arduino, char *idata){
	uint8 ibuf = 0;
	char cbuf[2];
	arduino->readByte(ibuf);
	sprintf_s(cbuf, "%c", ibuf);
	*idata = cbuf[0];
	return ibuf == 0 ? false : true;
}

void IntroQuiz::run()
{
	//Music
	Array<String> musics_name;
	Array<String> musics_path = FileSystem::DirectoryContents(L"IntroGameSound");
	Sound* musics_sound = new Sound();
	for (int cnt = 0; cnt < musics_path.size(); cnt++){
		if (FileSystem::IsFile(musics_path.at(cnt))){
			musics_name.emplace_back(FileSystem::BaseName(musics_path.at(cnt)));
			//musics_sound.emplace_back(Sound(musics_path.at(cnt)));
		}
		else
		{
			musics_path.erase(musics_path.begin() + cnt);
			cnt--;
		}
	}
	//Graphic
	Graphics::SetBackground(Palette::White);
	const s3d::Font minifont(10);
	const s3d::Font normalfont(30);
	const s3d::Font bigfont(100);
	static const Size button_size = { 20, 20 };
	static const Size button_flame_size = { button_size.x + button_size.x*0.25, button_size.y + button_size.y*0.25 };
	Array<Rect> music_play_button;
	Array<Rect> music_stop_button;
	for (int cnt = 0; cnt < musics_name.size(); cnt++){
		music_play_button.emplace_back(Rect(0, cnt*button_flame_size.y, button_size));
		music_stop_button.emplace_back(Rect(button_flame_size.x, cnt*button_flame_size.y, button_size).draw(Palette::White));
	}

	//ini
	String player_name[8];
	INIReader inir(L"config.ini");
	if (!inir)return;
	for (int i = 0; i < 8; i++)player_name[i] = inir.get<String>(Format(L"PLAYERNAME.", (i + 1)));
	bool push_nth[8] = { 0 };

	//パソコンの画面サイズを取得
	const s3d::Size pc_size = { GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN) };
	//フルスクリーンにする
	Window::SetVirtualFullscreen(pc_size);
	//早押しボタン(マイコン)の初期化
	s3d::Serial arduino(3);

	//サウンドの読み込み
	const s3d::Sound se_push_answerbutton(L"Assets/sound/push_answerbutton.wav");
	const s3d::Sound se_correct_answer(L"Assets/sound/correct_answer.wav");
	const s3d::Sound se_incorrect_answer(L"Assets/sound/incorrect_answer.wav");

	//init
	for (int i = 0; i < 8; i++) push_nth[i] = 0;

	bool mode_pause_flag = false;
	bool mode_name_change_flag = false;
	int name_change_nth = -1;

	//開始
	while (s3d::System::Update())
	{
		//キー入力を確認　X...正解音 Y...不正解音
		if (!mode_name_change_flag){
			if (mode_pause_flag) if (s3d::Input::KeyX.clicked)se_correct_answer.playMulti();
			if (mode_pause_flag) if (s3d::Input::KeyC.clicked)se_incorrect_answer.playMulti();
			if (!mode_pause_flag) if (s3d::Input::KeyQ.pressed){
				musics_sound->stop();
				break;
			}
			int input_key;
			if ((input_key = 1 * s3d::Input::Key1.clicked) ||
				(input_key = 2 * s3d::Input::Key2.clicked) ||
				(input_key = 3 * s3d::Input::Key3.clicked) ||
				(input_key = 4 * s3d::Input::Key4.clicked) ||
				(input_key = 5 * s3d::Input::Key5.clicked) ||
				(input_key = 6 * s3d::Input::Key6.clicked) ||
				(input_key = 7 * s3d::Input::Key7.clicked) ||
				(input_key = 8 * s3d::Input::Key8.clicked)
				){
				s3d::System::Update();//入力リセット
				player_name[input_key - 1] = L"";
				mode_name_change_flag = true;
				name_change_nth = input_key - 1;
			}
		}

		if (mode_name_change_flag){
			Input::GetCharsHelper(player_name[name_change_nth]);
			if (s3d::Input::KeyEnter.clicked){
				//INI書き込み
				INIReader inir(L"config.ini");
				INIWriter iniw(L"config.ini");
				for (int i = 0; i < 8; i++) iniw.write(L"PLAYERNAME", Format(i + 1), inir.get<String>(Format(L"PLAYERNAME.", (i + 1))));
				iniw.write(L"PLAYERNAME", Format(name_change_nth + 1), player_name[name_change_nth]);
				mode_name_change_flag = false;
				iniw.close();
			}
		}
		else if (mode_pause_flag){
			//リセットボタンが押されたので解除を試みる
			if (s3d::Input::KeyV.clicked){
				//解除コードを送信
				arduino.writeByte('1');
				//返答が来るまでまつ
				char res = NULL;
				while (!arduinoGet(&arduino, &res)){}
				switch (res)
				{
				case 'C'://成功
					//一時停止していた音楽を再生する
					if(!musics_sound->isEmpty) musics_sound->play();
					//init
					for (int i = 0; i < 8; i++) push_nth[i] = 0;
					mode_pause_flag = false;
					break;
				case 'F'://失敗
					break;
				default:
					break;
				}
			}
		}
		else{
			//再生|停止ボタンが押されたか
			for (int cnt = 0; cnt < musics_name.size(); cnt++){
				if (music_play_button.at(cnt).leftClicked){
					musics_sound->stop();
					musics_sound= new Sound(musics_path.at(cnt));
					musics_sound->play();
				}
				if (music_stop_button.at(cnt).leftClicked){
					musics_sound->release();
				}
			}
			//早押しボタンが押されたか
			char c_arduino_ans = NULL;
			int i_arduino_ans = -1;
			if (arduinoGet(&arduino, &c_arduino_ans)){
				i_arduino_ans = (int)c_arduino_ans - 48;
				if (0 <= i_arduino_ans&&i_arduino_ans < 8) push_nth[i_arduino_ans] = 1;
				//プッシュ音再生
				se_push_answerbutton.playMulti();
				//音楽を一時停止
				musics_sound->pause();
				//リセットボタンが押されるまでpause
				mode_pause_flag = true;
			}
		}

		/*描画*/
		//Music
		//再生ボタン、停止ボタン、名前
		for (int cnt = 0; cnt < musics_name.size(); cnt++){
			music_play_button.at(cnt).draw(Palette::White);
			music_stop_button.at(cnt).draw(Palette::White);
			minifont(L"▶").draw(5, cnt*button_flame_size.y, Palette::Darkblue);
			minifont(L"■").draw(button_flame_size.x + 5, cnt*button_flame_size.y, Palette::Darkblue);
			minifont(musics_name.at(cnt)).draw(2 * button_flame_size.x, cnt*button_flame_size.y, Palette::Black);
		}
		//ポーズライト
		Circle(80, 990, 40).draw(mode_pause_flag ? Palette::Red : Palette::Seagreen);
		//ボタン
		for (int i = 0; i < 8; i++){
			//外枠
			Rect(i * 240, 990, 240, 90).draw(Palette::Black);
			//内枠
			Rect(i * 240 + 5, 995, 230, 80).draw(push_nth[i] ? Palette::Limegreen : Palette::Skyblue);
			//ネームプレート
			normalfont(player_name[i]).draw(i * 240 + 30, 1005, Palette::Black, 3);
		}
	}
}
