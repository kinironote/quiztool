﻿#include <Siv3D.hpp>
#include <curl/curl.h>
#include <math.h>
#include <string>
#include <Windows.h>
#include <string>

#include "MosaicQuiz.h"
#include "SimpleQuiz.h"
#include "IntroQuiz.h"

void Menu(MosaicQuiz* MQ,SimpleQuiz* SQ,IntroQuiz* IQ);

void Main(){
	MosaicQuiz mosaicQuiz;
	SimpleQuiz simpleQuiz;
	IntroQuiz introQuiz;

	Menu(&mosaicQuiz,&simpleQuiz,&introQuiz);
	
	return;
}

void Menu(MosaicQuiz* MQ, SimpleQuiz* SQ, IntroQuiz* IQ)
{
	GUI gui(GUIStyle::Default);
	gui.setTitle(L"QuizTool");
	//gui.addln(GUIText::Create(L""));
	gui.add(L"MQ", GUIButton::Create(L"モザイク"));
	gui.add(L"SQ", GUIButton::Create(L"シンプル"));
	gui.add(L"IQ", GUIButton::Create(L"イントロ"));
	gui.setCenter(Window::Center() - s3d::Point(0,1000));


	auto init = [&](){
		Graphics::SetBackground(Palette::Burlywood);
		gui.show(true);
		//普通のスクリーンにする
		Window::SetFullscreen(false, {});
		Window::Resize({ 400, 70 });
		Window::SetStyle(s3d::WindowStyle::Fixed);
	};

	init();

	while (System::Update())
	{
		if (gui.button(L"MQ").pushed)
		{
			gui.show(false);
			MQ->run();
			init();
		}
		else if (gui.button(L"SQ").pushed)
		{
			gui.show(false);
			SQ->run();
			init();
		}
		else if (gui.button(L"IQ").pushed)
		{
			gui.show(false);
			IQ->run();
			init();
		}
	}
}