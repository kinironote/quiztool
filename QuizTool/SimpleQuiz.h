#pragma once
class SimpleQuiz
{
public:
	SimpleQuiz();
	~SimpleQuiz();
	void run();
private:
	//Arduinoからの応答(1byte)を受け取る
	bool arduinoGet(s3d::Serial *arduino, char *idata);
};
