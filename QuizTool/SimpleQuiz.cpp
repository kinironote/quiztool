#include <Siv3D.hpp>
#include <curl/curl.h>
#include <math.h>
#include <string>
#include <Windows.h>
#include <string>

#include "SimpleQuiz.h"

SimpleQuiz::SimpleQuiz()
{
}


SimpleQuiz::~SimpleQuiz()
{
}

//Arduinoからの応答(1byte)を受け取る
bool SimpleQuiz::arduinoGet(s3d::Serial *arduino, char *idata){
	uint8 ibuf = 0;
	char cbuf[2];
	arduino->readByte(ibuf);
	sprintf_s(cbuf, "%c", ibuf);
	*idata = cbuf[0];
	return ibuf == 0 ? false : true;
}

void SimpleQuiz::run()
{
	//Graphic
	Graphics::SetBackground(Palette::White);
	const s3d::Font normalfont(30);
	const s3d::Font bigfont(100);
	String player_name[8];
	INIReader inir(L"config.ini");
	if (!inir)return;
	for (int i = 0; i < 8; i++)player_name[i] = inir.get<String>(Format(L"PLAYERNAME.", (i + 1)));
	bool push_nth[8] = { 0 };

	//パソコンの画面サイズを取得
	const s3d::Size pc_size = { GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN) };
	//普通のスクリーンにする
	Window::SetFullscreen(false, { 640, 480 });
	Window::SetStyle(s3d::WindowStyle::Fixed);
	//早押しボタン(マイコン)の初期化
	s3d::Serial arduino(3);

	//サウンドの読み込み
	const s3d::Sound se_push_answerbutton(L"Assets/sound/push_answerbutton.wav");
	const s3d::Sound se_correct_answer(L"Assets/sound/correct_answer.wav");
	const s3d::Sound se_incorrect_answer(L"Assets/sound/incorrect_answer.wav");

	//init
	for (int i = 0; i < 8; i++) push_nth[i] = 0;

	bool mode_pause_flag = false;
	bool mode_name_change_flag = false;
	int name_change_nth = -1;

	//開始
	while (s3d::System::Update())
	{
		//キー入力を確認　X...正解音 Y...不正解音
		if (!mode_name_change_flag){
			if (mode_pause_flag) if (s3d::Input::KeyX.clicked)se_correct_answer.playMulti();
			if (mode_pause_flag) if (s3d::Input::KeyC.clicked)se_incorrect_answer.playMulti();
			if (!mode_pause_flag) if (s3d::Input::KeyQ.pressed)break;
			int input_key;
			if ((input_key = 1 * s3d::Input::Key1.clicked) ||
				(input_key = 2 * s3d::Input::Key2.clicked) ||
				(input_key = 3 * s3d::Input::Key3.clicked) ||
				(input_key = 4 * s3d::Input::Key4.clicked) ||
				(input_key = 5 * s3d::Input::Key5.clicked) ||
				(input_key = 6 * s3d::Input::Key6.clicked) ||
				(input_key = 7 * s3d::Input::Key7.clicked) ||
				(input_key = 8 * s3d::Input::Key8.clicked)
				){
				s3d::System::Update();//入力リセット
				player_name[input_key - 1] = L"";
				mode_name_change_flag = true;
				name_change_nth = input_key - 1;
			}
		}

		if (mode_name_change_flag){
			Input::GetCharsHelper(player_name[name_change_nth]);
			if (s3d::Input::KeyEnter.clicked){
				//INI書き込み
				INIReader inir(L"config.ini");
				INIWriter iniw(L"config.ini");
				for (int i = 0; i < 8; i++) iniw.write(L"PLAYERNAME", Format(i + 1), inir.get<String>(Format(L"PLAYERNAME.", (i + 1))));
				iniw.write(L"PLAYERNAME", Format(name_change_nth + 1), player_name[name_change_nth]);
				mode_name_change_flag = false;
				iniw.close();
			}
		}else if (mode_pause_flag){
			//リセットボタンが押されたので解除を試みる
			if (s3d::Input::KeyV.clicked){
				//解除コードを送信
				arduino.writeByte('1');
				//返答が来るまでまつ
				char res = NULL;
				while (!arduinoGet(&arduino, &res)){}
				switch (res)
				{
				case 'C'://成功
					//init
					for (int i = 0; i < 8; i++) push_nth[i] = 0;
					mode_pause_flag = false;
					break;
				case 'F'://失敗
					break;
				default:
					break;
				}
			}
		}
		else{
			//早押しボタンが押されたか
			char c_arduino_ans = NULL;
			int i_arduino_ans = -1;
			if (arduinoGet(&arduino, &c_arduino_ans)){
				i_arduino_ans = (int)c_arduino_ans - 48;
				if (0 <= i_arduino_ans&&i_arduino_ans < 8) push_nth[i_arduino_ans] = 1;
				//プッシュ音再生
				se_push_answerbutton.playMulti();
				//リセットボタンが押されるまでpause
				mode_pause_flag = true;
			}
		}

		/*描画*/
		//ポーズライト
		Circle(500, 400, 40).draw(mode_pause_flag ? Palette::Red : Palette::Seagreen);
		//ボタン
		for (int y = 0; y < 3; y++){
			for (int x = 0; x < 3; x++){
				if (y == 2 && x == 2)break;
				//外枠
				Rect(x * 214, y * 100, 212, 90).draw(push_nth[y * 3 + x] ? Palette::Limegreen : Palette::Skyblue);
				//ネームプレート
				normalfont(player_name[y * 3 + x]).draw(x * 214 + 5, y * 100 + 10, Palette::Black, 3);
			}
		}
	}
}

