#pragma once
class MosaicQuiz
{
public:
	MosaicQuiz();
	~MosaicQuiz();
	void run();
private:
	//レンタルサーバーのupdate_ans.phpにGETで解答を送る(ここ(雑):http://acf.php.xdomain.jp/tools/ans.php)
	void SendAnswer(int ans_num);
	//Arduinoからの応答(1byte)を受け取る
	bool arduinoGet(s3d::Serial *arduino, char *idata);
};
