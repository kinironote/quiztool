#include <Siv3D.hpp>
#include <curl/curl.h>
#include <math.h>
#include <string>
#include <Windows.h>
#include <string>

#include "MosaicQuiz.h"

//最初のモザイクの度数
#define MOSAIC_MAX 100

MosaicQuiz::MosaicQuiz()
{
}


MosaicQuiz::~MosaicQuiz()
{
}

//レンタルサーバーのupdate_ans.phpにGETで解答を送る(ここ(雑):http://acf.php.xdomain.jp/tools/ans.php)
void MosaicQuiz::SendAnswer(int ans_num){
	static CURL *curl;
	static char send_url[256];
	sprintf_s(send_url, "http://acf.php.xdomain.jp/tools/update_ans.php?ans=%d", ans_num);
	curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_URL, send_url);
	curl_easy_perform(curl);//ここで送信
	curl_easy_cleanup(curl);
}

//Arduinoからの応答(1byte)を受け取る
bool MosaicQuiz::arduinoGet(s3d::Serial *arduino, char *idata){
	uint8 ibuf = 0;
	char cbuf[2];
	arduino->readByte(ibuf);
	sprintf_s(cbuf, "%c", ibuf);
	*idata = cbuf[0];
	return ibuf == 0 ? false : true;
}

void MosaicQuiz::run()
{
	//Graphic
	const s3d::Font normalfont(30);
	const s3d::Font bigfont(100);
	String player_name[8];
	INIReader inir(L"config.ini");
	if (!inir)return;
	for (int i = 0; i < 8; i++)player_name[i] = inir.get<String>(Format(L"PLAYERNAME.", (i + 1)));
	//for (int i = 0; i < 8; i++)player_name[i]=L"player"+std::to_wstring(i+1);
	bool push_nth[8] = { 0 };

	//パソコンの画面サイズを取得
	const s3d::Size pc_size = { GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN) };
	//フルスクリーンにする
	Window::SetVirtualFullscreen(pc_size);
	//cURL初期化
	curl_global_init(CURL_GLOBAL_DEFAULT);
	//早押しボタン(マイコン)の初期化
	s3d::Serial arduino(3);
	//モザイク画像を格納する場所
	s3d::DynamicTexture picmosaic;

	//サウンドの読み込み
	const s3d::Sound se_push_answerbutton(L"Assets/sound/push_answerbutton.wav");
	const s3d::Sound se_correct_answer(L"Assets/sound/correct_answer.wav");
	const s3d::Sound se_incorrect_answer(L"Assets/sound/incorrect_answer.wav");

	//ポケモン画像のデータ
	const int picmosaic_width = 588;
	const int picmosaic_height = 588;
	const int picmosaic_cut_width = 231; //このくらいがいいかなーって適当に決めた、画像の切り取り幅（拡大の度合い)
	const int picmosaic_cut_height = (int)(((double)picmosaic_cut_width / (double)pc_size.x) * pc_size.y); //PCのサイズは色々あるので、widthを元に計算する PC_width : Cut_width = PC_height : Cut_height
	const int picmosaic_cut_y_max = picmosaic_height - picmosaic_cut_height;
	const int picmosaic_cut_x_max = picmosaic_width - picmosaic_cut_width;

	//１ループ１ポケモン
	while (s3d::System::Update()){
		if (s3d::Input::KeyQ.pressed)break;
		//init
		for (int i = 0; i < 8; i++) push_nth[i] = 0;

		int timer = 0;
		int mosaic_level = MOSAIC_MAX;
		bool mode_pause_flag = false;
		bool mode_name_change_flag = false;
		int name_change_nth = -1;
		int picmosaic_y_before = 0;//移動前の座標
		int picmosaic_x_before = 0;
		int picmosaic_y_next = 0;//移動先の座標
		int picmosaic_x_next = 0;
		int picmosaic_y = 0;//現在の座標
		int picmosaic_x = 0;

		//ポケモンを720匹の中からランダムで決める
		int pokemon_num = (int)(s3d::Random() * 720) + 1;
		s3d::String pokemon_name = s3d::Format(L"Assets/picture/pokemon/" + s3d::Pad(pokemon_num, { 3, L'0' }) + L".png");
		s3d::Image pokemon_pic = s3d::Image(pokemon_name);

		//サーバーに解答を送る(正解はスマホで見る)
		SendAnswer(pokemon_num);

		//最初の座標が(0,0)だと不自然なのでランダムにする
		picmosaic_x_next = (int)(s3d::Random() * picmosaic_cut_x_max);
		picmosaic_y_next = (int)(s3d::Random() * picmosaic_cut_y_max);

		//開始
		while (s3d::System::Update())
		{
			//キー入力を確認　X...正解音 Y...不正解音 Z...次の問題へ
			if (!mode_name_change_flag){
				if (mode_pause_flag) if (s3d::Input::KeyX.clicked)se_correct_answer.playMulti();
				if (mode_pause_flag) if (s3d::Input::KeyC.clicked)se_incorrect_answer.playMulti();
				if (!mode_pause_flag) if (s3d::Input::KeyZ.clicked)break;
				if (!mode_pause_flag) if (s3d::Input::KeyQ.pressed)break;
				int input_key;
				if ((input_key = 1 * s3d::Input::Key1.clicked) ||
					(input_key = 2 * s3d::Input::Key2.clicked) ||
					(input_key = 3 * s3d::Input::Key3.clicked) ||
					(input_key = 4 * s3d::Input::Key4.clicked) ||
					(input_key = 5 * s3d::Input::Key5.clicked) ||
					(input_key = 6 * s3d::Input::Key6.clicked) ||
					(input_key = 7 * s3d::Input::Key7.clicked) ||
					(input_key = 8 * s3d::Input::Key8.clicked)
					){
					s3d::System::Update();//入力リセット
					player_name[input_key - 1] = L"";
					mode_name_change_flag = true;
					name_change_nth = input_key - 1;
				}
			}

			if (mode_name_change_flag){
				Input::GetCharsHelper(player_name[name_change_nth]);
				if (s3d::Input::KeyEnter.clicked){
					//INI書き込み
					INIReader inir(L"config.ini");
					INIWriter iniw(L"config.ini");
					for (int i = 0; i < 8; i++) iniw.write(L"PLAYERNAME", Format(i + 1), inir.get<String>(Format(L"PLAYERNAME.", (i + 1))));
					iniw.write(L"PLAYERNAME", Format(name_change_nth + 1), player_name[name_change_nth]);
					mode_name_change_flag = false;
					iniw.close();
				}
			}
			else if (mode_pause_flag){
				//リセットボタンが押されたので解除を試みる
				if (s3d::Input::KeyV.clicked){
					//解除コードを送信
					arduino.writeByte('1');
					//返答が来るまでまつ
					char res = NULL;
					while (!arduinoGet(&arduino, &res)){}
					switch (res)
					{
					case 'C'://成功
						//init
						for (int i = 0; i < 8; i++) push_nth[i] = 0;
						mode_pause_flag = false;
						break;
					case 'F'://失敗
						break;
					default:
						break;
					}
				}
			}
			else{
				//早押しボタンが押されたか
				char c_arduino_ans = NULL;
				int i_arduino_ans = -1;
				if (arduinoGet(&arduino, &c_arduino_ans)){
					i_arduino_ans = (int)c_arduino_ans - 48;
					if (0 <= i_arduino_ans&&i_arduino_ans < 8) push_nth[i_arduino_ans] = 1;
					//プッシュ音再生
					se_push_answerbutton.playMulti();
					//リセットボタンが押されるまでpause
					mode_pause_flag = true;
				}
				//120tickごとに画像の移動先を新たに設定する
				if (timer % 120 == 0){
					picmosaic_x_before = picmosaic_x_next;
					picmosaic_y_before = picmosaic_y_next;
					while (1){
						//ランダムに移動先地点を設定
						picmosaic_x_next = (int)(s3d::Random() * picmosaic_cut_x_max);
						picmosaic_y_next = (int)(s3d::Random() * picmosaic_cut_y_max);
						//ランダムに移動させると全く動かなかったり同じ所しか見えないことが多いので、大きく移動するような値になるまで繰り返す
						if (abs(picmosaic_x_next - picmosaic_x_before) > picmosaic_cut_x_max / 2 || abs(picmosaic_y_next - picmosaic_y_before) > picmosaic_cut_y_max / 2) break; //ｸｿｺｰﾄﾞ
					}
				}
				//6tickごとにモザイクを細かくしていく
				if (timer % 6 == 0){
					//終盤はゆっくりになるように調整
					if (mosaic_level < 30){
						if (timer % 18 == 0){
							mosaic_level -= 1;
						}
					}
					else{
						mosaic_level -= 1;
					}
					//picmosaicに、モザイクが掛けられた画像を格納
					picmosaic.fill(pokemon_pic.mosaiced(mosaic_level, mosaic_level));
				}
				//画像の移動
				picmosaic_y = picmosaic_y_before + (int)(((picmosaic_y_next - picmosaic_y_before) / 120.0) * (timer % 120));
				picmosaic_x = picmosaic_x_before + (int)(((picmosaic_x_next - picmosaic_x_before) / 120.0) * (timer % 120));
				timer++;
			}

			/*描画*/
			//モザイク画像
			picmosaic(picmosaic_x, picmosaic_y, picmosaic_cut_width, picmosaic_cut_height).resize(pc_size.x, pc_size.y).drawAt(pc_size.x / 2, pc_size.y / 2);
			//ボタン
			for (int i = 0; i < 8; i++){
				Rect(i * 240, 990, 240, 90).draw(s3d::Palette::Black);
				if (push_nth[i]) Rect(i * 240 + 5, 995, 230, 80).draw(s3d::Palette::Limegreen);
				else Rect(i * 240 + 5, 995, 230, 80).draw(s3d::Palette::Silver);
				normalfont(player_name[i]).draw(i * 240 + 30, 1005, Palette::Black, 3);
				//bigfont(push_nth[i]? L"!":L"").draw(i * 240 + 100, 900, Palette::Black, 3);
			}
		}
	}
}

