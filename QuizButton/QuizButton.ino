#define PULLHIGH 0
#define PULLLOW 1

const int iPins[9] = {13, 11, 9, 7, 5, 3, 14, 16, 18};
const int oPins[9] = {12, 10, 8, 6, 4, 2, 15, 17, 19};

void setup() {
  for (int i = 0; i < 9; i++) {
    pinMode(iPins[i], INPUT_PULLUP);
    pinMode(oPins[i], OUTPUT);
    digitalWrite(oPins[i], LOW);
  }
  Serial.begin(9600);
}

void loop() {
  static bool isPush = false;
  static int pushed_button_num;
  if (isPush == false) {
    //Check push
    for (int i = 0; i < 9; i++) {
      if (digitalRead(iPins[i]) == PULLHIGH) {
        isPush = true;
        digitalWrite(oPins[i], HIGH);
        pushed_button_num = i;
        Serial.print(pushed_button_num);
        break;
      }
    }
  } else if (isPush == true) {
    char idata = Serial.read();
    switch(idata){
      case -1:
      {
        break;
      }
      case '1':
      {
        //すべてのボタンが押されていなければ終了
        bool flag = true;
        for (int i = 0; i < 9; i++) {
          if (digitalRead(iPins[i]) == PULLHIGH) flag = false;
        }
        if (flag == false) {
          //失敗
          Serial.print("F");
        } else {
          //正常終了
          Serial.print("C");
          for (int i = 0; i < 9; i++) {
            digitalWrite(oPins[i], LOW);
          }
          isPush = false;
        }
        break;
      }
      default:
      {
        //Serial.println(N);
        break;
      }
    }
  }
}
